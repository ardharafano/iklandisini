<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iklandisini.com</title>

    <!-- Meta -->
    <meta name="description" content="Iklandisini.com">
    <meta name="keywords" content="Iklandisini.com">
    <meta property="og:title" content="Iklandisini.com" />
    <meta property="og:description" content="Beriklan Mudah Tanpa Ribet" />
    <meta property="og:url" content="iklandisini.com" />
    <meta property="og:image" content="https://www.iklandisini.com/assets/images/logo_iklandisini_natal_2022.svg" />
    <!-- End Meta  -->

    <!-- Favicon -->
    <link rel="Shortcut icon" href="assets/images/favicon.png" />
    <!-- End Favicon -->

    <!-- Bootstrap  -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css?v=1" />
    <!-- End Bootstrap  -->

    <!-- Owl  -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css" />
    <!-- End Owl  -->

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@3.6.6/dist/css/splide.min.css">

    <!-- My Style -->
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <!-- End My Style -->

    <!-- splidejs -->
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
    <link rel="stylesheet" href="assets/css/splide.min.css">
    <!-- splidejs -->
    <!-- <script nonce="random123" src="https://trusted.example.com/trusted_script.js"></script> -->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

</head>

<body>

    <!-- Navbar -->
    <?php if(isset($_GET['page'])) 
        { 
            if($_GET['page'] != 'login') 
            if($_GET['page'] != 'register') 
            if($_GET['page'] != 'forgot_password')
            if($_GET['page'] != 'profile')  
        { 
        ?>
        
        <?php include('include/blocks/navbar.php'); ?>
        <div class="distance-top"></div>

            <?php } } else { ?>
                <?php include('include/blocks/navbar.php'); ?>
                <div class="distance-top"></div>
            <?php 
        }  
    ?>
    <!-- End Navbar -->


    <?php
        if(isset($_GET['page'])){ 
            $url = 'index.php?';
            include("include/pages/".$_GET['page'].".php");
        }else{
            $url = 'index.php?';
            include("include/pages/home.php");
        }
    ?>

        <!-- Footer -->
        <?php if(isset($_GET['page'])) 
        { 
            if($_GET['page'] != 'login') 
            if($_GET['page'] != 'register') 
            if($_GET['page'] != 'forgot_password') 
            if($_GET['page'] != 'profile') 
        { 
        ?>
        <?php include('include/blocks/footer.php'); ?>
            <?php } } else { ?>
                <?php include('include/blocks/footer.php'); ?>
            <?php 
        }  
    ?>
        <!-- End Footer -->

        <!-- JQuery -->
        <!-- <script src="assets/js/jquery-3.3.1.min.js"></script> -->
        <script src="https://code.jquery.com/jquery-3.6.1.slim.min.js" integrity="sha256-w8CvhFs7iHNVUtnSP0YKEg00p9Ih13rlL9zGqvLdePA=" crossorigin="anonymous"></script>
        <!-- End JQuery -->

        <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3.6.6/dist/js/splide.min.js"></script>

        <!-- Custom Js -->
        <script src="assets/js/main.js"></script>
        <!-- End Custom Js -->

        <!-- Bootstrap  -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- End Bootstrap  -->

        <!-- Owl -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- End Owl -->

        <script>
            $('#iklan-baris').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: true,
                dots: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    1000: {
                        items: 4
                    }
                }
            })
            $('#media-owl').owlCarousel({
                loop: true,
                margin: 10,
                nav: false,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    1000: {
                        items: 1
                    }
                }
            })
            $('#display-owl').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                dots: false,
                autoplay: false,
                navText: ["<span class='icon-svg icon-left'></span>", "<span class='icon-svg icon-right'></span>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    700: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });
            $('#display-owl2').owlCarousel({
                loop: true,
                margin: 10,
                nav: true,
                dots: false,
                autoplay: false,
                navText: ["<span class='icon-svg icon-left'></span>", "<span class='icon-svg icon-right'></span>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    700: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });
            if ($('#main-slider').length) {
                var main = new Splide('#main-slider', {
                    type: 'fade',
                    heightRatio: 0.5,
                    pagination: false,
                    arrows: false,
                    cover: true,
                });

                var thumbnails = new Splide('#thumbnail-slider', {
                    rewind: true,
                    fixedWidth: 104,
                    fixedHeight: 58,
                    isNavigation: true,
                    gap: 10,
                    focus: 'left',
                    pagination: false,
                    cover: true,
                    dragMinThreshold: {
                        mouse: 4,
                        touch: 10,
                    },
                    breakpoints: {
                        640: {
                            fixedWidth: 66,
                            fixedHeight: 38,
                        },
                    },
                });

                main.sync(thumbnails);
                main.mount();
                thumbnails.mount();
            }
        </script>

</body>

</html>