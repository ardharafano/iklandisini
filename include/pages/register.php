<div id="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 left">
                <img src="assets/images/logo-iklandisini.svg" alt="iklandisini.com" class="logo" />
                <h1>REGISTER</h1>
                <form action="?page=login" method="post" class="d-flex justify-content-center row g-3">

                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Username or Email">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Depan">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Belakang">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="No Handphone">
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>Jenis Kelamin</option>
                                <option>Laki - Laki</option>
                                <option>Perempuan</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tanggal Lahir">
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>Interest</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>Provinsi</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>Kota</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Password">
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Konfirmasi Password">
                        </div>
                    </div>

                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Saya setuju dengan <a class="syarat" href="#">Syarat & Ketentuan</a></label>
                    </div>

                    <button type="submit" class="button">DAFTAR</button>

                </form>

                <div>
                    <p class="mt-5">Sudah Memiliki Akun ?</p>
                    <a class="link" href="?page=login">Masuk</a>
                </div>
                <div class="created">
                    <p>Created by</p>
                    <a class="link" href="#"><img src="assets/images/arkadiame.png" alt="img" class="arkadiame" /></a>
                </div>

            </div>

            <div class="col-lg-6">
                <img src="assets/images/bg-login.png" alt="img" class="bg" />
            </div>
        </div>
    </div>
</div>