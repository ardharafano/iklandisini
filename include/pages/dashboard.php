<?php // include('include/components/header-'.$_GET['page'].'.php'); ?>

<!-- Dashboard  -->
<section class="dashboard mb-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-12 mb-md-0 mb-4">

            <h1 class="h3 mb-1 fw-bold">Dashboard</h1>
            <p>Silahkan atur sendiri Iklanmu sesuai kebutuhanmu</p>

                <div class="side">
                    <h3>Status</h3>
                    <ul>
                        <li>
                            <a href="#">
                                <span>
                                    <i class="icon-svg icon-active"></i> 
                                    Aktif
                                </span>
                                <span>
                                    0
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>
                                    <i class="icon-svg icon-waiting"></i> 
                                    Menunggu
                                </span>
                                <span>
                                    0
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>
                                    <i class="icon-svg icon-expired"></i> 
                                    Kadaluarsa
                                </span>
                                <span>
                                    0
                                </span>
                            </a>
                        </li>
                    </ul>
                    <h3>Aksi</h3>
                    <ul>
                        <li>
                            <a href="#">
                                <span>
                                    <i class="icon-svg icon-order-ads"></i> 
                                    Order Iklan Lainnya
                                </span>
                                <span class="icon-svg icon-right"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8 col-12">
                <div class="d-flex w-100 flex-wrap mb-3">
                    <a href="#" class="btn-in-dashboard mb-md-0 mb-3">
                        Digital
                    </a>
                    <a href="#" class="btn-in-dashboard active">
                        Advetorial
                    </a>
                </div>
                <div class="d-flex w-100 flex-wrap mb-5">
                    <input type="text" class="filter-order" placeholder="Cari Tanggal Order" />
                </div>
                <div class="table-responsive">
                    <table class="table table-custom-dashboard">
                        <thead>
                            <tr>
                                <td>
                                    No
                                </td>
                                <td>
                                    Tgl Order
                                </td>
                                <td>
                                    Jenis Iklan
                                </td>
                                <td>
                                    Tipe Iklan
                                </td>
                                <td>
                                    Mulai Aktif
                                </td>
                                <td>
                                    Berakhir
                                </td>
                                <td>
                                    Status
                                </td>
                                <td>
                                    Aksi
                                </td>
                                <td>
                                    Rincian
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    1
                                </td>
                                <td>
                                    Test
                                </td>
                                <td>
                                    Test
                                </td>
                                <td>
                                    Test
                                </td>
                                <td>
                                    Test
                                </td>
                                <td>
                                    Test
                                </td>
                                <td>
                                    Test
                                </td>
                                <td>
                                    Test
                                </td>
                                <td>
                                    Test
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            
            </div>
        </div>
    </div>
</section>
<!-- End Dashboard  -->