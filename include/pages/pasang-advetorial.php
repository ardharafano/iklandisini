<?php include('include/components/transaction/header-'.$_GET['page'].'.php'); ?>

<!-- Form -->
<section class="form-transaction">
    <div class="container">
        <form class="w-100">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Package</label>
                        <select class="input-transaction">
                            <option value="">Pilih Package</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Title</label>
                        <input type="text" class="input-transaction" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Brand</label>
                        <input type="text" class="input-transaction" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Description</label>
                        <textarea type="text" class="input-transaction description"></textarea>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Backlink</label>
                        <input type="text" class="input-transaction" />
                    </div>
                </div>
                <div class="col-md-6 col-12"></div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <span>Image</span>
                        <label for="file-img" class="btn-file">Choose File</label>
                        <input type="file" id="file-img" class="btn-file" />
                        <ul>
                            <li>* png, jpg, gif</li>
                            <li>* max image (1 MB)</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <span>File</span>
                        <label for="file-doc" class="btn-file">Choose File</label>
                        <input type="file" id="file-doc" class="btn-file" />
                        <ul>
                            <li>* doc, docx</li>
                            <li>* max file (1 MB)</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row bg-none">
                <div class="col-12 d-flex justify-content-center flex-md-nowrap flex-wrap">
                    <a href="?page=review-advetorial" class="btn-done">
                        Next
                    </a>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- End Form -->