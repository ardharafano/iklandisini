<div class="mt-5">
    <div class="col-12">
        <h2 class="text-center">Iklan Baris</h2>
        <h6 class="text-center mb-5">Kami percaya setiap bisnis berbeda, untuk itu pasang & atur sendiri iklanmu</h6>
    </div>
</div>
<div class="iklan-baris-detail">
    <div class="iklan-baris-detail-desc">
        <div class="bg-side-absolute"></div>
        <div class="bg-side-absolute1"></div>
        <div class="wrap-iklan-baris">
            <div id="main-slider" class="splide">
                <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80">
                            </li>
                            <li class="splide__slide">
                                <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80">
                            </li>
                            <li class="splide__slide">
                                <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80">
                            </li>
                        </ul>
                </div>
            </div>
            <div id="thumbnail-slider" class="splide mt-2">
                <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80">
                            </li>
                            <li class="splide__slide">
                                <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80">
                            </li>
                            <li class="splide__slide">
                                <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80">
                            </li>
                        </ul>
                </div>
            </div>
        </div>
        <div class="iklan-baris-detail-desc-wrap mt-5">
            <div class="iklan-baris-detail-desc-wrap-title">Deskripsi</div>
            <div>
                <p>BMW 520i F10 Modern<br />Luxury Facelift LCI 2014<br />NIK 2014<br />Odo 85rb miles<br />Pajak 01 2022<br />Plat Genap B 1810 NAB<br />Service record<br />Alpine White<br />Facelift LCI<br />Bonus:<br />Free premium detailing interior exterior dan mesin.<br />Garansi mesin dan transmisi 30hari by Otospector<br />Garansi sertifikat Otospector mobil bebas tabrakan dan banjir.<br />Spec:<br />Engine 2,000cc TwinPower Turbo<br />Bensin super irit 1/13<br />Full orisinil<br />Brown leather interior<br />Sunroof</p>
            </div>
        </div>
        <div class="iklan-baris-detail-desc-wrap mt-3">
            <div class="iklan-baris-detail-desc-wrap-title">Iklan Terkait</div>
            <div class="iklan-baris-detail-desc-terkait">
                <div class="card-iklan-baris">
                    <div class="card-iklan-baris-img">
                        <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" alt="" srcset="">
                        <div class="card-iklan-baris-love">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                            </svg>
                        </div>
                    </div>
                    <div class="card-iklan-baris-info">
                        <div class="card-iklan-baris-price">Rp 250.000.000</div>
                        <div class="card-iklan-baris-title">Mistubishi XPender Merah Tahun 2018</div>
                    </div>
                    <div class="card-iklan-baris-location">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                            <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                            <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        </svg>
                        <div class="card-iklan-baris-name">Kebayoran Lama, Jakarta Selatan...</div>
                    </div>
                </div>
                <div class="card-iklan-baris">
                    <div class="card-iklan-baris-img">
                        <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" alt="" srcset="">
                        <div class="card-iklan-baris-love">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                            </svg>
                        </div>
                    </div>
                    <div class="card-iklan-baris-info">
                        <div class="card-iklan-baris-price">Rp 250.000.000</div>
                        <div class="card-iklan-baris-title">Mistubishi XPender Merah Tahun 2018</div>
                    </div>
                    <div class="card-iklan-baris-location">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                            <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                            <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        </svg>
                        <div class="card-iklan-baris-name">Kebayoran Lama, Jakarta Selatan...</div>
                    </div>
                </div>
                <div class="card-iklan-baris">
                    <div class="card-iklan-baris-img">
                        <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" alt="" srcset="">
                        <div class="card-iklan-baris-love">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                            </svg>
                        </div>
                    </div>
                    <div class="card-iklan-baris-info">
                        <div class="card-iklan-baris-price">Rp 250.000.000</div>
                        <div class="card-iklan-baris-title">Mistubishi XPender Merah Tahun 2018</div>
                    </div>
                    <div class="card-iklan-baris-location">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                            <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                            <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        </svg>
                        <div class="card-iklan-baris-name">Kebayoran Lama, Jakarta Selatan...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="iklan-baris-detail-sidebar">
        <div class="iklan-baris-detail-card-info">
            <div class="iklan-baris-detail-card-info-title">Mitsubishi X Pender (2018)</div>
            <div class="iklan-baris-detail-card-info-desc">2.0 Modern Bensin</div>
            <hr style="color:#fff">
            <div>
                <div class="iklan-baris-detail-card-info-list">
                    <img src="assets/images/iklan-baris/bbm.svg" alt="" srcset="">
                    <div>BENSIN</div>
                </div>
                <div class="iklan-baris-detail-card-info-list">
                    <img src="assets/images/iklan-baris/km.svg" alt="" srcset="">
                    <div>80.000-85.000 KM</div>
                </div>
                <div class="iklan-baris-detail-card-info-list">
                    <img src="assets/images/iklan-baris/transmission.svg" alt="" srcset="">
                    <div>AUTOMATIC</div>
                </div>
            </div>
        </div>
        <div class="iklan-baris-detail-card-info mt-3">
            <div class="iklan-baris-detail-card-info-harga mb-2">Rp 250.000.000</div>
            <div class="iklan-baris-detail-card-info-love">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                </svg>
            </div>
            <div class="iklan-baris-detail-card-info-penawaran">Buat Penawaran</div>
        </div>
        <div class="iklan-baris-detail-card-info mt-3">
            <div class="iklan-baris-detail-card-title">Deskripsi penjual</div>
            <div class="mt-2">
                <div class="seller-card">
                    <img class="seller-card-img" src="https://images.unsplash.com/photo-1491609154219-ffd3ffafd992?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=100&h=100&q=80" alt="" srcset="">
                    <div class="seller-card-info">
                        <div class="seller-card-name">Bambang</div>
                        <div class="seller-card-registered text-muted">Anggota sejak Jan 2015</div>
                    </div>
                </div>
                <div class="seller-card-date text-muted mt-3">Diposting Pada : 08/11/2021</div>
                <div class="seller-card-chat mt-2">Chat dengan penjual</div>
                <div class="seller-card-phone-card mt-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-telephone" viewBox="0 0 16 16">
                        <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
                    </svg>
                    <div class="seller-card-phone-card-phone">xx xxxx xxxx</div>
                    <div class="seller-card-phone-card-show">Tampilkan nomor</div>
                </div>
            </div>
        </div>
        <div class="iklan-baris-detail-card-info mt-3">
            <div class="iklan-baris-detail-card-title">Lokasi iklan</div>
            <div class="iklan-baris-detail-card-title-desc text-muted">Kebayoran Lama, Jakarta Selatan, Jakarta D.K.I.</div>
            <iframe class="mt-2" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15865.102973717745!2d106.82963755!3d-6.2273296!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3f5dab27d45%3A0x2e8ddc0ba471ecb1!2sApartemen%20Taman%20Rasuna!5e0!3m2!1sid!2sid!4v1638248554092!5m2!1sid!2sid" width="100%" height="150" style="border:0;" allowfullscreen="false" loading="lazy"></iframe>
        </div>
    </div>
</div>

