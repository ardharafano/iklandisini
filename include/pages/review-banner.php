<?php include('include/components/transaction/header-'.$_GET['page'].'.php'); ?>

<!-- Form -->
<section class="form-transaction">
    <div class="container">
        <form class="w-100">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="box mb-md-0 mb-5">
                        <img class="default-img img-responsive w-100" src="assets/images/default-img.png" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box mb-md-0 mb-5">
                        <ul>
                            <li>Landing Page : www.iklandisini.com</li>
                            <li>Jenis Iklan	: Mobile</li>
                            <li>Iklan Tampil : 2021-03-02</li>
                            <li>Akhir Tampil : 2021-03-05</li>
                            <li>Impresi	: 507.075</li>
                            <li>Harga Iklan	: Rp. 6.084.900</li>
                        </ul>
                        <a href="?page=payment" class="btn-file mt-3">
                            Pesan
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- End Form -->