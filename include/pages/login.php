<div id="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 left">
                <img src="assets/images/logo-iklandisini.svg" alt="iklandisini.com" class="logo" />
                <h1>SIGN IN</h1>
                <form action="profile.php" method="post">
                    <div class="form-group mb-3">
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Username or Email">
                    </div>
                    <div class="form-group mb-3">
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    
                    <button type="submit" class="button">LOGIN</button>
                </form>

                <div>
                    <a class="link" href="?page=forgot_password">Lupa Password</a>
                    <p>Belum Memiliki Akun ?</p>
                    <a class="link" href="?page=register">Daftar Akun</a>
                </div>

                <div class="created">
                    <p>Created by</p>
                    <a class="link" href="#"><img src="assets/images/arkadiame.png" alt="img" class="arkadiame" /></a>
                </div>

            </div>

            <div class="col-lg-6">
                <img src="assets/images/bg-login.png" alt="img" class="bg" />
            </div>
        </div>
    </div>
</div>