<div id="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 left">
                <img src="assets/images/logo-iklandisini.svg" alt="iklandisini.com" class="logo" />
                <h1>Lupa Password</h1>
                <form action="?page=login" method="post"> 
                    <div class="form-group mb-3">
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Username or Email">
                    </div>
                    <button type="submit" class="button">RESET PASSWORD</button>
                </form>

                <div>
                    <p class="mt-5">Belum Memiliki Akun ?</p>
                    <a class="link" href="?page=register">Daftar Akun</a>
                </div>

                <div class="created">
                    <p>Created by</p>
                    <a class="link" href="#"><img src="assets/images/arkadiame.png" alt="img" class="arkadiame" /></a>
                </div>

            </div>

            <div class="col-lg-6">
                <img src="assets/images/bg-login.png" alt="img" class="bg" />
            </div>
        </div>
    </div>
</div>