<?php include('include/components/transaction/header-'.$_GET['page'].'.php'); ?>

<!-- Form -->
<section class="form-transaction">
    <div class="container">
        <form class="w-100">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Media</label>
                        <select class="input-transaction">
                            <option value="">Pilih Media</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Device</label>
                        <select class="input-transaction">
                            <option value="">Pilih Device</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Posisi</label>
                        <select class="input-transaction">
                            <option value="">Pilih Posisi</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Landingpage</label>
                        <input type="text" class="input-transaction" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <img class="default-img" src="assets/images/default-img.png" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label for="file-img" class="btn-file">Choose File</label>
                        <input type="file" id="file-img" class="btn-file" />
                        <ul>
                            <li>* png, jpg, gif, mp4</li>
                            <li>* max image (1 MB)</li>
                            <li>* max video (5 MB)</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row bg-none">
                <div class="col-12 d-flex justify-content-center flex-md-nowrap flex-wrap">
                    <a href="?page=plan" class="btn-done">
                        Next
                    </a>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- End Form -->