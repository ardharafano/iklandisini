<?php include('include/components/transaction/header-'.$_GET['page'].'.php'); ?>

<!-- Form -->
<section class="form-payment">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-12">
                <p>Segera lakukan pembayaran<br/> dalam waktu</p>
            </div>
            <div class="col-lg-6 col-12 pl-lg-5 pl-0">
                <span class="countdown d-block">34 Menit : 40 Detik</span>
                <span class="d-block">Sebelum 2021-03-1 16:10:</span>
            </div>
            <div class="col-12 d-flex mb-4 justify-content-md-between warning-block flex-md-nowrap flex-wrap justify-content-center">
                <img src="assets/images/other-icons/icon_warning.png" class="d-inline-block mb-md-0 mb-3" />
                <p class="d-inline-block">Pastikan untuk tidak menginformasikan bukti dan data pembayaran kepada pihak manapun kecuali iklandisini.com</p>
            </div>
            <div class="col-lg-9 col-12 no-rek border-bottom pb-4 mb-4">
                <span>Transfer pembayaran ke Bank BCA, nomor rekening:</span>
                <h2>0703850500</h2>
                <span>a/n PT Integra Archipelago Media</span>
            </div>
            <div class="col-lg-3 col-12 border-bottom pb-4 mb-4 d-flex justify-content-lg-start justify-content-center">
                <a href="#" class="btn-salin">
                    Salin
                </a>
            </div>
            <div class="col-lg-9 col-12 no-rek border-bottom pb-4 mb-4">
                <span>Jumlah yang harus dibayar:</span>
                <h3>RP. 6.085.663</h3>
                <div class="info">
                    <span>
                        <b>Transfer tepat sampai 3 digit terakhir</b><br/>
                        Perbedaan digit akan menghambat verifikasi
                    </span>
                </div>
            </div>
            <div class="col-lg-3 col-12 border-bottom pb-4 mb-4 d-flex justify-content-lg-start justify-content-center">
                <a href="#" class="btn-salin">
                    Salin
                </a>
            </div>
            <div class="col-12 d-flex justify-content-center flex-md-nowrap flex-wrap">
                <a href="?page=done" class="btn-done">
                    Konfirmasi Pembayaran
                </a>
                <a href="?page=dashboard" class="btn-done ms-md-5 ms-0">
                    Cek Status Pembayarann
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Form -->