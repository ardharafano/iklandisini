<?php include('include/components/transaction/header-'.$_GET['page'].'.php'); ?>

<!-- Form -->
<section class="form-transaction">
    <div class="container">
        <form class="w-100">
            <div class="row">
                <div class="col-12 border-bottom pb-4 mb-5 justify-content-md-start">
                    <h2 class="h4 text-start">Durasi Pasang Iklan</h2>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Dari Tanggal</label>
                        <input type="date"  class="input-transaction" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Berakhir Tanggal</label>
                        <input type="date"  class="input-transaction" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <div class="d-flex justify-content-between">
                            <span>Impresi Iklan</span>
                            <span>Max: 2.000.000</span>
                        </div>
                        <input class="input-transaction" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Total Impresi</label>
                        <input type="text" class="input-transaction" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <label>Total Harga</label>
                        <input type="text" class="input-transaction" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="box">
                        <br/>
                        <a href="?page=review-banner" class="btn-file mt-3">
                            Next
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- End Form -->