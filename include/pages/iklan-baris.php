<?php include('include/components/header.php'); ?>

<!-- Layanan -->
<section class="baris p-md-5 px-2 py-5">
    <div class="bg-side-absolute"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">Iklan Baris</h2>
                <h6 class="text-center mb-5">Kami percaya setiap bisnis berbeda, untuk itu pasang & atur sendiri iklanmu</h6>
            </div>
        </div>
        <div class="row bg-custom">
            <div class="owl-carousel owl-theme" id="iklan-baris">
                <div class="item">
                    <div class="card-iklan-baris">
                        <div class="card-iklan-baris-img">
                            <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" alt="img" srcset="">
                            <div class="card-iklan-baris-love">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                </svg>
                            </div>
                        </div>
                        <div class="card-iklan-baris-info">
                            <div class="card-iklan-baris-price">Rp 250.000.000</div>
                            <div class="card-iklan-baris-title">Mistubishi XPender Merah Tahun 2018</div>
                        </div>
                        <div class="card-iklan-baris-location">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                                <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                                <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                            </svg>
                            <div class="card-iklan-baris-name">Kebayoran Lama, Jakarta Selatan...</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card-iklan-baris">
                        <div class="card-iklan-baris-img">
                            <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" alt="img" srcset="">
                            <div class="card-iklan-baris-love">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                </svg>
                            </div>
                        </div>
                        <div class="card-iklan-baris-info">
                            <div class="card-iklan-baris-price">Rp 250.000.000</div>
                            <div class="card-iklan-baris-title">Mistubishi XPender Merah Tahun 2018</div>
                        </div>
                        <div class="card-iklan-baris-location">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                                <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                                <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                            </svg>
                            <div class="card-iklan-baris-name">Kebayoran Lama, Jakarta Selatan...</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card-iklan-baris">
                        <div class="card-iklan-baris-img">
                            <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" alt="img" srcset="">
                            <div class="card-iklan-baris-love">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                </svg>
                            </div>
                        </div>
                        <div class="card-iklan-baris-info">
                            <div class="card-iklan-baris-price">Rp 250.000.000</div>
                            <div class="card-iklan-baris-title">Mistubishi XPender Merah Tahun 2018</div>
                        </div>
                        <div class="card-iklan-baris-location">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                                <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                                <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                            </svg>
                            <div class="card-iklan-baris-name">Kebayoran Lama, Jakarta Selatan...</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card-iklan-baris">
                        <div class="card-iklan-baris-img">
                            <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" alt="img" srcset="">
                            <div class="card-iklan-baris-love">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                </svg>
                            </div>
                        </div>
                        <div class="card-iklan-baris-info">
                            <div class="card-iklan-baris-price">Rp 250.000.000</div>
                            <div class="card-iklan-baris-title">Mistubishi XPender Merah Tahun 2018</div>
                        </div>
                        <div class="card-iklan-baris-location">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                                <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                                <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                            </svg>
                            <div class="card-iklan-baris-name">Kebayoran Lama, Jakarta Selatan...</div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card-iklan-baris">
                        <div class="card-iklan-baris-img">
                            <img src="https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" alt="img" srcset="">
                            <div class="card-iklan-baris-love">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                </svg>
                            </div>
                        </div>
                        <div class="card-iklan-baris-info">
                            <div class="card-iklan-baris-price">Rp 250.000.000</div>
                            <div class="card-iklan-baris-title">Mistubishi XPender Merah Tahun 2018</div>
                        </div>
                        <div class="card-iklan-baris-location">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                                <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                                <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                            </svg>
                            <div class="card-iklan-baris-name">Kebayoran Lama, Jakarta Selatan...</div>
                        </div>
                    </div>
                </div>
            
            </div>
        
        </div>
    </div>
</section>
<!-- End Layanan -->
<!-- Pasang Iklan  -->
<section class="pasang py-5">
    <div class="bg-side-absolute"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center text-white">Pasang Iklan</h2>
                <h6 class="text-center text-white mb-5">Kami menyediakan cara beriklan yang bisa kamu sesuaikan berdasarkan kebutuhan</h6>
            </div>
        </div>
        <div class="row d-flex flex-wrap justify-content-center">
            <div class="col-lg-4 col-md-6 col-12 mb-lg-3 mb-4 d-flex flex-wrap justify-content-center">
                <div class="box">
                    <img src="assets/images/other-icons/display1.png" />
                    <h3 class="text-center">Display Banner</h3>
                    <span class="d-block text-center">Banyak pilihan iklan untuk media</span>
                    <a href="?page=desktop" class="btn-detail-display">
                        Selengkapnya
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mb-lg-3 mb-4 d-flex flex-wrap justify-content-center">
                <div class="box">
                    <img src="assets/images/other-icons/display2.png" />
                    <h3 class="text-center">Advetorial</h3>
                    <span class="d-block text-center">Iklan berupa berita</span>
                    <a href="?page=advetorial" class="btn-detail-display">
                        Selengkapnya
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mb-lg-3 mb-4 d-flex flex-wrap justify-content-center">
                <div class="box">
                    <img src="assets/images/other-icons/display3.png" />
                    <h3 class="text-center">Sosmed</h3>
                    <span class="d-block text-center">Banyak pilihan iklan untuk sosial media</span>
                    <a href="?page=sosmed" class="btn-detail-display">
                        Selengkapnya
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mb-lg-3 mb-4 d-flex flex-wrap justify-content-center">
                <div class="box">
                    <img src="assets/images/other-icons/display4.png" />
                    <h3 class="text-center">Video</h3>
                    <span class="d-block text-center">Pembuatan Video dan lainnya</span>
                    <a href="?page=video" class="btn-detail-display">
                        Selengkapnya
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mb-lg-3 mb-4 d-flex flex-wrap justify-content-center">
                <div class="box">
                    <img src="assets/images/other-icons/display5.png" />
                    <h3 class="text-center">Display TV</h3>
                    <span class="d-block text-center">Pembuatan Display TV</span>
                    <a href="?page=tv" class="btn-detail-display">
                        Selengkapnya
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Pasang Iklan  -->
<!-- Keuntungan  -->
<section class="keuntungan p-md-5 px-2 py-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">Keuntungan</h2>
                <h6 class="text-center mb-5">Temukan berbagai kemudahan dan keuntungan dengan beriklan di Iklandisini.com</h6>
            </div>
        </div>
        <div class="row d-flex flex-wrap justify-content-center list-keuntungan">
            <div class="col-lg-3 col-md-6 col-12 mb-lg-0 mb-3">
                <div class="box">
                    <div class="thumb">
                        <img src="assets/images/other-icons/keuntungan1.png" />
                    </div>
                    <div class="des">
                        <h2>Dilihat Jutaan Pembaca</h2>
                        <span>Berpotensi dilihat oleh jutaan pembaca media digital.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12 mb-lg-0 mb-3">
                <div class="box">
                    <div class="thumb">
                        <img src="assets/images/other-icons/keuntungan2.png" />
                    </div>
                    <div class="des">
                        <h2>Tampil di Media Terkeren</h2>
                        <span>Iklan kamu akan tampil di Group Media terkeren dan terpercaya di Indonesia.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12 mb-lg-0 mb-3">
                <div class="box">
                    <div class="thumb">
                        <img src="assets/images/other-icons/keuntungan3.png" />
                    </div>
                    <div class="des">
                        <h2>Harga Terjangkau</h2>
                        <span>Kamu dapat mempromosikan iklan kamu di sini mulai dari Rp 50.000.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12 mb-lg-0 mb-3">
                <div class="box">
                    <div class="thumb">
                        <img src="assets/images/other-icons/keuntungan4.png" />
                    </div>
                    <div class="des">
                        <h2>Dikendalikan Penuh</h2>
                        <span>Kamu dapat mengatur iklan sesuai dengan kebutuhan usaha kamu.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Keuntungan  -->
<!-- Layanan -->
<section class="layanan p-md-5 px-2 py-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">Layanan Pemasangan Iklan<br/> Secara Mandiri</h2>
                <h6 class="text-center mb-5">Kami percaya setiap bisnis berbeda, untuk itu pasang & atur sendiri iklanmu</h6>
            </div>
        </div>
        <div class="row bg-custom">
            <div class="col-lg-4 col-12 d-flex justify-content-center">
                <div class="box">
                    <div class="thumb d-flex align-items-center justify-content-center flex-column">
                        <img src="assets/images/other-icons/layanan1.png" />
                    </div>
                    <h2>Pilih Sendiri</h2>
                    <span>Kamu bisa memilih sendiri jenis<br/>
                    iklan yang sesuai dengan<br/>
                    kebutuhan usahamu.</span>
                </div>
            </div>
            <div class="col-lg-4 col-12  d-flex justify-content-center">
                <div class="box">
                    <div class="thumb d-flex align-items-center justify-content-center flex-column">
                        <img src="assets/images/other-icons/layanan2.png" />
                    </div>
                    <h2>Pasang Sendiri</h2>
                    <span>Kamu yang paling mengerti<br/> 
                    konsumenmu. Pasang iklan<br/> 
                    sesuai kebutuhanmu.</span>
                </div>       
            </div>
            <div class="col-lg-4 col-12 d-flex justify-content-center">
                <div class="box">
                    <div class="thumb d-flex align-items-center justify-content-center flex-column">
                        <img src="assets/images/other-icons/layanan3.png" />
                    </div>
                    <h2>Pantau Sendiri</h2>
                    <span>Pantau status dan performa<br/> 
                    iklanmu di dashboard untuk<br/> 
                    digital & sosmed.</span>
                </div>   
            </div>
        </div>
    </div>
</section>
<!-- End Layanan -->
<?php include('include/components/price.php'); ?>
<?php include('include/components/media.php'); ?>
<?php include('include/components/faqs.php'); ?>