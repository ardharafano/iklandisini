<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">
        <!-- Menu -->

        <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
            <div class="app-brand demo">
                <a href="index.php" class="app-brand-link" target="_blank">
                    <img src="assets/images/logo-iklandisini.png" alt="iklandisini.com" width="191" height="23" />
                </a>

                <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                    <i class="bx bx-chevron-left bx-sm align-middle"></i>
                </a>
            </div>

            <div class="menu-inner-shadow"></div>

            <ul class="menu-inner py-1">
                <!-- Dashboard -->
                <li class="menu-item">
                    <a href="profile.php" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-home-circle"></i>
                        <div data-i18n="Analytics">Dashboard</div>
                    </a>
                </li>


                <li class="menu-header small text-uppercase">
                    <span class="menu-header-text">Pasang Iklan</span>
                </li>

                <li class="menu-item">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-detail"></i>
                        <div data-i18n="Account Settings">User</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=user-kode-referral" class="menu-link">
                                <div data-i18n="Account">Kode Referral</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=pages-pasang-banner" class="menu-link">
                                <div data-i18n="Notifications">Pesan Iklan Lainnya</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=user-pembayaran-advertorial" class="menu-link">
                                <div data-i18n="Connections">Pembayaran Advertorial</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=user-classified-ads" class="menu-link">
                                <div data-i18n="Connections">Classified Ads</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=user-kritik-dan-saran" class="menu-link">
                                <div data-i18n="Connections">Kritik & Saran</div>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class='menu-icon tf-icons  bx bxs-bell'></i>
                        <div data-i18n="Account Settings">Status</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=dashboard-user" class="menu-link">
                                <div data-i18n="Account">Aktif (0)</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=dashboard-user" class="menu-link">
                                <div data-i18n="Notifications">Menunggu (0)</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=dashboard-user" class="menu-link">
                                <div data-i18n="Connections">Kadaluarsa (0)</div>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-detail"></i>
                        <div data-i18n="Account Settings">Business</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=business-akun-bisnis-step-1" class="menu-link">
                                <div data-i18n="Account">Daftar Akun Bisnis</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=business-transaksi-sosmed" class="menu-link">
                                <div data-i18n="Notifications">Transaksi Sosmed</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=business-laporan-sosmed" class="menu-link">
                                <div data-i18n="Connections">Laporan Akun Sosmed</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=business-pembayaran-sosmed" class="menu-link">
                                <div data-i18n="Connections">Pembayaran Akun Sosmed</div>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item">
                    <a href="?profile=pages-topup" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-money"></i>
                        <div data-i18n="Analytics">Top Up</div>
                    </a>
                </li>

                <li class="menu-item active open">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons  bx bx-file-find"></i>
                        <div data-i18n="Account Settings">Jenis Iklan</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=pages-pasang-banner" class="menu-link">
                                <div data-i18n="Account">Display Banner</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=pages-pasang-advertorial" class="menu-link">
                                <div data-i18n="Notifications">Advertorial</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=pages-pasang-sosmed" class="menu-link">
                                <div data-i18n="Connections">Sosmed</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=pages-pasang-video" class="menu-link">
                                <div data-i18n="Connections">Video</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=pages-pasang-tv" class="menu-link">
                                <div data-i18n="Connections">Display TV</div>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Components -->




                <!-- Forms & Tables -->
                <li class="menu-header small text-uppercase"><span class="menu-header-text">Setting</span></li>
                <li class="menu-item">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-lock-open-alt"></i>
                        <div data-i18n="Account Settings">Account Settings</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=pages-account-settings-account" class="menu-link">
                                <div data-i18n="Account">Account</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=pages-account-settings-password" class="menu-link">
                                <div data-i18n="Notifications">Password</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=pages-account-settings-notifications" class="menu-link">
                                <div data-i18n="Notifications">Notifications</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=pages-account-settings-connections" class="menu-link">
                                <div data-i18n="Connections">Social Media</div>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-header small text-uppercase"><span class="menu-header-text">Suara Network</span></li>
                <li class="menu-item">
                    <a href="https://suara.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_suara.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Suara.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://matamata.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_matamata.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Matamata.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://yoursay.suara.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_yoursay.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Yoursay.id</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://www.bolatimes.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_bolatimes.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Bolatimes.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://hitekno.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_hitekno.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Hitekno.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://www.dewiku.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_dewiku.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Dewiku.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://www.mobimoto.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_mobimoto.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Mobimoto.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://guideku.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_guideku.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Guideku.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://www.himedik.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_himedik.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">HiMedik.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://clickmov.suara.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_clickmov.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Clickmov.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://serbada.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_serbada.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Serbada.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://hits.suara.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_suara.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Beritahits.id</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://www.iklandisini.com/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_iklandisini.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Iklandisini.com</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="https://www.theindonesia.id/" class="menu-link" target="_blank">
                        <img src="assets/vendor/images/partners/icon_theindonesia.png" alt="img" class="me-1" />
                        <div data-i18n="Analytics">Theindonesia.id</div>
                    </a>
                </li>
                </li>
        </aside>
        <!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">
            <!-- Navbar -->

            <nav class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                    <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                        <i class="bx bx-menu bx-sm"></i>
                    </a>
                </div>

                <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                    <!-- Search -->
                    <!-- <div class="navbar-nav align-items-center">
                        <img src="assets/vendor/images/arkadia.svg" alt="Arkadia.me" class="arkadiame" />
                    </div> -->
                    <div class="navbar-nav align-items-center">
                        <div class="nav-item d-flex align-items-center">
                            <i class="bx bx-search fs-4 lh-0"></i>
                            <input type="text" class="form-control border-0 shadow-none" placeholder="Search..." aria-label="Search..." />
                        </div>
                    </div>
                    <!-- /Search -->

                    <div class="dropdown language">
                        <element class="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="assets/vendor/images/idn.png" alt="img" width="20" height="auto" />
                        </element>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="dropdown-item" href="#"><img src="assets/vendor/images/uk.png" alt="img" width="20" height="auto" class="me-2" /> English</a>
                            </li>
                        </ul>
                    </div>

                    <ul class="navbar-nav flex-row align-items-center ms-auto">
                        <!-- Place this tag where you want the button to render. -->
                        <li class="nav-item lh-1 me-3">
                            <img src="assets/vendor/images/faq.svg" alt="img" width="20" height="auto" />
                        </li>

                        <li class="nav-item lh-1 me-3">
                            <img src="assets/vendor/images/chat.svg" alt="img" width="20" height="auto" />
                        </li>

                        <!-- User -->
                        <li class="nav-item navbar-dropdown dropdown-user dropdown">
                            <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                                <div class="avatar avatar-online">
                                    <img src="assets/vendor/images/1.png" alt class="w-px-40 h-auto rounded-circle" />
                                </div>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li>
                                    <a class="dropdown-item" href="#">
                                        <div class="d-flex">
                                            <div class="flex-shrink-0 me-3">
                                                <div class="avatar avatar-online">
                                                    <img src="assets/vendor/images/1.png" alt class="w-px-40 h-auto rounded-circle" />
                                                </div>
                                            </div>
                                            <div class="flex-grow-1">
                                                <span class="fw-semibold d-block">John Doe</span>
                                                <small class="text-muted">Admin</small>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <div class="dropdown-divider"></div>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="?profile=home">
                                        <i class="bx bx-user me-2"></i>
                                        <span class="align-middle">My Profile</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="?profile=pages-account-settings-account">
                                        <i class="bx bx-cog me-2"></i>
                                        <span class="align-middle">Settings</span>
                                    </a>
                                </li>
                                <!-- <li>
                                    <a class="dropdown-item" href="#">
                                        <span class="d-flex align-items-center align-middle">
                      <i class="flex-shrink-0 bx bx-credit-card me-2"></i>
                      <span class="flex-grow-1 align-middle">Billing</span>
                                        <span class="flex-shrink-0 badge badge-center rounded-pill bg-danger w-px-20 h-px-20">4</span>
                                        </span>
                                    </a>
                                </li> -->
                                <li>
                                    <div class="dropdown-divider"></div>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="index.php?page=login">
                                        <i class="bx bx-power-off me-2"></i>
                                        <span class="align-middle">Log Out</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!--/ User -->
                    </ul>
                </div>
            </nav>

            <!-- / Navbar -->

            <!-- Content wrapper -->
            <div class="content-wrapper">
                <!-- Dashboard  -->
                <div class="container-xxl flex-grow-1 container-p-y">
                    <div class="card mb-4">
                        <hr class="my-0" />

                        <header>
                            <div class="container">
                                <div class="row">
                                    <h1 class="h3 mb-1 ps-5 fw-bold mt-5">Review</h1>
                                    <p class="ps-5 mb-5">Review order kamu untuk memastikannya</p>
                                </div>
                            </div>
                        </header>
                        <!-- Form -->
                        <section class="form-transaction">
                            <div class="container">
                                <form class="w-100">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="box mb-md-0 mb-5">
                                                <img class="default-img img-responsive w-100" src="assets/images/default-img.png" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="box mb-md-0 mb-5">
                                                <ul>
                                                    <li>Landing Page : www.iklandisini.com</li>
                                                    <li>Jenis Iklan : Mobile</li>
                                                    <li>Iklan Tampil : 2021-03-02</li>
                                                    <li>Akhir Tampil : 2021-03-05</li>
                                                    <li>Impresi : 507.075</li>
                                                    <li>Harga Iklan : Rp. 6.084.900</li>
                                                </ul>
                                                <a href="?profile=pages-payment" class="btn-file mt-3">
                            Pesan
                        </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                        <!-- End Form -->


                    </div>
                </div>
                <!-- End Dashboard  -->


                <!-- Footer -->

                <!-- / Footer -->

                <div class="content-backdrop fade"></div>
            </div>
            <!-- Content wrapper -->
        </div>
        <!-- / Layout page -->
    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->