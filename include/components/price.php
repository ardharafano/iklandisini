<!-- Price -->
<section class="price mb-5 text-white" id="harga">
    <div class="container py-5">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">Harga</h2>
                <h6 class="text-center mb-5">Pilih iklan sesuai kebutuhanmu</h6>
            </div>
        </div>
        <div class="row px-md-3 px-0 card-custom mx-2">
            <div class="col-md-4 col-12 d-flex justify-content-center align-center flex-column">
                <h5 class="mx-md-4 mx-3 mb-md-0 mb-4 text-md-start text-center pl-md-3">Display Banner</h5>
            </div>
            <div class="col-md-8 col-12">
                <div class="owl-carousel owl-theme" id="display-owl">
                    <div class="item">
                        <h5>Desktop</h5>
                        <p>
                            Leaderboard 
                        </p>
                        <p> 
                            (975x100)
                        </p>
                        <p>
                            <b>Rp 10.000</b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                    <div class="item">
                        <h5>Desktop</h5>
                        <p>
                            Medium Rectangle 
                        </p>
                        <p> 
                            (300x250)
                        </p>
                        <p>
                            <b>Rp 9.000</b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                    <div class="item">
                        <h5>Desktop</h5>
                        <p>
                            Skin Ads (Sticky)
                        </p>
                        <p> 
                            (160x600)
                        </p>
                        <p>
                            <b>Rp 12.000</b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                    <div class="item">
                        <h5>Desktop</h5>
                        <p>
                            Leaderboard 
                        </p>
                        <p> 
                            (975x100)
                        </p>
                        <p>
                            <b>Rp 10.000</b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                    <div class="item">
                        <h5>Desktop</h5>
                        <p>
                            Medium Rectangle 
                        </p>
                        <p> 
                            (300x250)
                        </p>
                        <p>
                            <b>Rp 9.000</b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                    <div class="item">
                        <h5>Desktop</h5>
                        <p>
                            Skin Ads (Sticky)
                        </p>
                        <p> 
                            (160x600)
                        </p>
                        <p>
                            <b>Rp 12.000</b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row px-md-3 px-0 card-custom mx-2 mt-4">
            <div class="col-md-4 col-12 d-flex justify-content-center align-center flex-column">
                <h5 class="mx-md-4 mx-3 mb-md-0 mb-4 text-md-start text-center pl-md-3">Advertorial</h5>
            </div>
            <div class="col-md-8 col-12">
                <div class="owl-carousel owl-theme" id="display-owl2">
                    <div class="item">
                        <h5>Advertorial, Create Article</h5>
                        <p>
                            <b>Rp.15.000.000 </b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                    <div class="item">
                        <h5>Advertorial, Create Article</h5>
                        <p>
                            <b>Rp.15.000.000 </b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                    <div class="item">
                        <h5>Advertorial, Create Article</h5>
                        <p>
                            <b>Rp.15.000.000 </b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                    <div class="item">
                        <h5>Advertorial, Create Article</h5>
                        <p>
                            <b>Rp.15.000.000 </b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                    <div class="item">
                        <h5>Advertorial, Create Article</h5>
                        <p>
                            <b>Rp.15.000.000 </b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                    <div class="item">
                        <h5>Advertorial, Create Article</h5>
                        <p>
                            <b>Rp.15.000.000 </b>
                        </p>
                        <p>
                            suara.com
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="row mx-md-0 mx-2 mt-4">
            <div class="col-md-8 col-12 mb-md-0 mb-4 px-md-2 px-0">
                <div class="card-custom mr-md-2">
                    <div class="row">
                        <div class="col-md-6 col-12 d-flex justify-content-center align-center flex-column">
                            <h5 class="mx-md-4 mx-3 mb-md-0 mb-4 text-md-start text-center pl-md-3">Advetorial</h5>
                        </div>
                        <div class="col-md-6 col-12">
                            <h5>Desktop & Mobile</h5>
                            <p>
                                Advetorial Ad (Iklan Artikel
                            </p>
                            <p> 
                                (1000x1000)
                            </p>
                            <p>
                                <b>Rp 8.000</b>
                            </p>
                            <p>
                                suara.com
                            </p>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- <div class="col-md-4 col-12 px-md-2 px-0">
                <div class="card-custom ml-md-2 d-flex justify-content-xl-between justify-content-md-center justify-content-between align-items-center flex-row flex-wrap">
                    <h5 class="mx-3 mb-0 pl-md-3">Sosmed</h5>
                    <a href="#">
                        <img src="assets/images/wa_chat.png" class="img-responsive" />
                    </a>
                </div>
            </div>
        </div> -->
        <div class="foot row mx-md-0 mx-2 mt-4">
        <div class="col-md-4 col-12 mb-md-0 mb-4 px-md-2 px-0">
                <div class="card-custom mr-md-2 d-flex justify-content-between align-items-center flex-row flex-wrap">
                    <h5 class="mx-md-4 mx-3 mb-0 text-md-start text-center">Sosmed</h5>
                    <a href="#">
                        <img src="assets/images/wa_chat.png" class="img-responsive" alt="img" />
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-12 mb-md-0 mb-4 px-md-2 px-0">
                <div class="card-custom mr-md-2 d-flex justify-content-between align-items-center flex-row flex-wrap">
                    <h5 class="mx-md-4 mx-3 mb-0 text-md-start text-center">Video</h5>
                    <a href="#">
                        <img src="assets/images/wa_chat.png" class="img-responsive" alt="img" />
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-12 px-md-2 px-0">
                <div class="card-custom ml-md-2 d-flex justify-content-between align-items-center flex-row flex-wrap">
                    <h5 class="mx-3 mb-0">Display TV</h5>
                    <!-- <a href="#" class="btn bg-red c-white">
                        Dummy
                    </a> -->
                    <a href="#">
                        <img src="assets/images/wa_chat.png" class="img-responsive" alt="img" />
                    </a>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- End Price -->