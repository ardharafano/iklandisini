<!-- Layanan -->
<section class="layanan p-md-5 py-5">

    <div class="container">
        <div class="row">

            <div class="col-12 mb-5">
                <h1>Layanan Pemasangan Iklan<br/> Secara Mandiri</h1>
                <h6 class="text-center">Kami percaya setiap bisnis berbeda, untuk itu pasang & atur sendiri iklanmu</h6>
            </div>

            <!-- <div class="bg d-flex flex-wrap align-content-center text-center mt-5">
                <div class="p-5 col-lg-4 col-12 bordered">
                    <div class="number">1</div>
                    <img src="assets/images/other-icons/layanan7.png" alt="img" class="layanan" />
                    <h2>Pilih Sendiri</h2>
                    <span>Kamu bisa memilih sendiri jenis<br/>
                    iklan yang sesuai dengan<br/>
                    kebutuhan usahamu.</span>
                </div>

                <div class="p-5 col-lg-4 col-12 bordered">
                    <div class="number">2</div>
                    <img src="assets/images/other-icons/layanan8.png" alt="img" class="layanan" />
                    <h2>Pasang Sendiri</h2>
                    <span>Kamu yang paling mengerti<br/> 
                    konsumenmu. Pasang iklan<br/> 
                    sesuai kebutuhanmu.</span>
                </div>
                <div class="p-5 col-lg-4 col-12 bordered">
                    <div class="number">3</div>
                    <img src="assets/images/other-icons/layanan9.png" alt="img" class="layanan" />
                    <h2>Pantau Sendiri</h2>
                    <span>Pantau status dan performa<br/> 
                    iklanmu di dashboard untuk<br/> 
                    digital & sosmed.</span></div>
            </div> -->

            <div class="col-lg-4">
                <div class="head-left">
                <div class="bg-left">
                    <img src="assets/images/other-icons/layanan7.png" alt="img" class="layanan1" />
                    <div class="number">1</div>
                    <h2>Pilih Sendiri</h2>
                    <span>Kamu bisa memilih sendiri jenis<br/>
                    iklan yang sesuai dengan<br/>
                    kebutuhan usahamu.</span>
                </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="head-center">
                <div class="bg-center">
                    <img src="assets/images/other-icons/layanan8.png" alt="img" class="layanan2" />
                    <div class="number">2</div>
                    <h2>Pasang Sendiri</h2>
                    <span>Kamu yang paling mengerti<br/> 
                    konsumenmu. Pasang iklan<br/> 
                    sesuai kebutuhanmu.</span>
                </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="head-right">
                <div class="bg-right">
                    <img src="assets/images/other-icons/layanan9.png" alt="img" class="layanan3" />
                    <div class="number">3</div>
                    <h2>Pantau Sendiri</h2>
                    <span>Pantau status dan performa<br/> 
                    iklanmu di dashboard untuk<br/> 
                    digital & sosmed.</span></div>
                </div>
                </div>
            </div>

        </div>
    </div>

</section>
<!-- End Layanan -->