<!-- Preview -->
<section class="preview mb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Jenis & Letak Banner Ads</h2>
                <span>Pilih jenis Ads banner yang kamu suka dan juga letak bannernya</span>
                <div class="select-preview">
                    <a href="?page=desktop" class="btn-preview">
                        Desktop
                    </a>
                    <a href="?page=mobile" class="btn-preview active">
                        Mobile
                    </a>
                    <select class="select-media">
                        <option value="">Pilih Media</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/large_rectangle.png" />
                    <span>
                        Large Rectangle (300x250)
                    </span>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/medium_rectangle.png" />
                    <span>
                        Medium Rectangle (300x250)
                    </span>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/small_rectangle.png" />
                    <span>
                        Small Rectangle (320x100)
                    </span>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/small_rectangle2.png" />
                    <span>
                        Small Rectangle (320x100)
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="?page=pasang-banner" class="btn-pasang-preview">
                    Pasang Sekarang
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Preview -->