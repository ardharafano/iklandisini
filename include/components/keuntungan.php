<!-- Keuntungan  -->
<section class="keuntungan" id="keuntungan">
    <div class="container desktop">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">Keuntungan</h2>
                <h6 class="text-center mb-5">Temukan berbagai kemudahan dan keuntungan dengan beriklan di Iklandisini.com </h6>
            </div>

            <div class="col-lg-6">
                <div class="p-5 border-keuntungan-1" style="border-radius:10px 0 0 0">
                    <h2 class="text-start text-white mt-4">Dilihat Jutaan Pembaca</h2>
                    <h6 class="text-start text-white mb-5">Berpotensi dilihat oleh jutaan pembaca media digital.</h6>
                </div>
            </div>
            <div class="col-lg-6">
                <img src="assets/images/other-icons/keuntungan5.png" alt="img" class="new1" style="border-radius:0 10px 0 0" />
            </div>

            <div class="col-lg-6">
                <img src="assets/images/other-icons/keuntungan6.png" alt="img" class="new2" />
            </div>
            <div class="col-lg-6">
                <div class="p-5 border-keuntungan-2">
                    <h2 class="text-start text-white mt-4">Tampil di Media Terkeren</h2>
                    <h6 class="text-start text-white mb-5">Iklan kamu akan tampil di Group Media terkeren dan terpercaya di Indonesia.</h6>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="p-5 border-keuntungan-1">
                    <h2 class="text-start text-white mt-4">Harga Terjangkau</h2>
                    <h6 class="text-start text-white mb-5">Kamu dapat mempromosikan iklan kamu di sini mulai dari Rp 50.000.</h6>
                </div>
            </div>
            <div class="col-lg-6">
                <img src="assets/images/other-icons/keuntungan7.png" alt="img" class="new1" />
            </div>

            <div class="col-lg-6">
                <img src="assets/images/other-icons/keuntungan8.png" alt="img" class="new2" style="border-radius:0 0 0 10px" />
            </div>
            <div class="col-lg-6">
                <div class="p-5 border-keuntungan-2" style="border-radius:0 0 10px 0">
                    <h2 class="text-start text-white mt-4">Dikendalikan Penuh</h2>
                    <h6 class="text-start text-white mb-5">Kamu dapat mengatur iklan sesuai dengan kebutuhan usaha kamu.</h6>
                </div>
            </div>

        </div>
    </div>

    <div class="container mobile">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">Keuntungan</h2>
                <h6 class="text-center mb-5">Temukan berbagai kemudahan dan keuntungan dengan beriklan di Iklandisini.com</h6>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6">
                <img src="assets/images/other-icons/keuntungan5.png" alt="img" class="new1" style="border-radius:10px 10px 0 0" />
            </div>
            <div class="col-lg-6">
                <div class="p-5 border-keuntungan-1">
                    <h2 class="text-start text-white mt-4">Dilihat Jutaan Pembaca</h2>
                    <h6 class="text-start text-white mb-5">Berpotensi dilihat oleh jutaan pembaca media digital.</h6>
                </div>
            </div>

            <div class="col-lg-6">
                <img src="assets/images/other-icons/keuntungan6.png" alt="img" class="new2" />
            </div>
            <div class="col-lg-6">
                <div class="p-5 border-keuntungan-2">
                    <h2 class="text-start text-white mt-4">Tampil di Media Terkeren</h2>
                    <h6 class="text-start text-white mb-5">Iklan kamu akan tampil di Group Media terkeren dan terpercaya di Indonesia.</h6>
                </div>
            </div>

            <div class="col-lg-6">
                <img src="assets/images/other-icons/keuntungan7.png" alt="img" class="new1" />
            </div>
            <div class="col-lg-6">
                <div class="p-5 border-keuntungan-1">
                    <h2 class="text-start text-white mt-4">Harga Terjangkau</h2>
                    <h6 class="text-start text-white mb-5">Kamu dapat mempromosikan iklan kamu di sini mulai dari Rp 50.000.</h6>
                </div>
            </div>


            <div class="col-lg-6">
                <img src="assets/images/other-icons/keuntungan8.png" alt="img" class="new2" />
            </div>
            <div class="col-lg-6">
                <div class="p-5 border-keuntungan-2" style="border-radius:0 0 10px 10px">
                    <h2 class="text-start text-white mt-4">Dikendalikan Penuh</h2>
                    <h6 class="text-start text-white mb-5">Kamu dapat mengatur iklan sesuai dengan kebutuhan usaha kamu.</h6>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Keuntungan  -->