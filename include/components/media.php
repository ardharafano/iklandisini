<!-- Media -->
<section class="media mb-5" id="media">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">Media</h2>
                <h6 class="text-center mb-5">Iklan akan tampil di group media terbesar di Indonesia</h6>
                <div class="owl-carousel owl-theme" id="media-owl">
                    <div class="item">
                        <img alt="suara.com" src="assets/images/partners/suara.png" alt="img" />
                        <img src="assets/images/partners/23-news.jpg" alt="img" />
                        <img src="assets/images/partners/ampera_co.jpg" alt="img" />
                        <img src="assets/images/partners/banten_hits.jpg" alt="img" />
                        <img src="assets/images/partners/banten_news.jpg" alt="img" />
                        <img src="assets/images/partners/batam_news.jpg" alt="img"/>
                        <img src="assets/images/partners/berita_bali.jpg" alt="img" />
                        <img src="assets/images/partners/covesia.jpg" alt="img" />
                        <img src="assets/images/partners/gamebrott.jpg" alt="img" />
                        <img src="assets/images/partners/goaceh.jpg" alt="img" />
                        <img src="assets/images/partners/gosulut.jpg" alt="img" />
                        <img src="assets/images/partners/gosumut.jpg" alt="img" />
                        <img src="assets/images/partners/harian_jogja.jpg" alt="img" />
                        <img src="assets/images/partners/jambiseru.jpg" alt="img" />
                        <img src="assets/images/partners/jatimnet.jpg" alt="img" />
                        <img src="assets/images/partners/joglosemar.jpg" alt="img" />
                    </div>
                    <div class="item">
                        <img src="assets/images/partners/kabar_makasar.jpg" alt="img" />
                        <img src="assets/images/partners/kabar_medan.jpg" alt="img" />
                        <img src="assets/images/partners/kabar_papua.jpg" alt="img" />
                        <img src="assets/images/partners/kabarnusa.jpg" alt="img" />
                        <img src="assets/images/partners/kalbar_update.jpg" alt="img" />
                        <img src="assets/images/partners/batam_news.jpg" alt="img" />
                        <img src="assets/images/partners/berita_bali.jpg" alt="img" />
                        <img src="assets/images/partners/covesia.jpg" alt="img" />
                        <img src="assets/images/partners/gamebrott.jpg" alt="img" />
                        <img src="assets/images/partners/goaceh.jpg" alt="img" />
                        <img src="assets/images/partners/gosulut.jpg" alt="img" />
                        <img src="assets/images/partners/gosumut.jpg" alt="img" />
                        <img src="assets/images/partners/harian_jogja.jpg" alt="img"/>
                        <img src="assets/images/partners/jambiseru.jpg" alt="img"/>
                        <img src="assets/images/partners/jatimnet.jpg" alt="img"/>
                        <img src="assets/images/partners/joglosemar.jpg" alt="img"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Media -->