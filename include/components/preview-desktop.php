<!-- Preview -->
<section class="preview mb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Jenis & Letak Banner Ads</h2>
                <span>Pilih jenis Ads banner yang kamu suka dan juga letak bannernya</span>
                <div class="select-preview">
                    <a href="?page=desktop" class="btn-preview active">
                        Desktop
                    </a>
                    <a href="?page=mobile" class="btn-preview">
                        Mobile
                    </a>
                    <select class="select-media">
                        <option value="">Pilih Media</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/leaderboard.png" />
                    <span>
                        Leaderboard (975x100)
                    </span>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/rectangle.png" />
                    <span>
                        Leaderboard (975x100)
                    </span>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/skin_left.png" />
                    <span>
                        Leaderboard (975x100)
                    </span>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/skin_right.png" />
                    <span>
                        Leaderboard (975x100)
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="?page=pasang-banner" class="btn-pasang-preview">
                    Pasang Sekarang
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Preview -->