<!-- Header  -->
<header class="header py-5">
    <!-- <div class="bg-side-header"></div> -->
    <div class="container">
        <div class="row">

            <section class="splide" aria-label="Splide Basic HTML Example">
                <div class="splide__track">
                    <ul class="splide__list">
                        <li class="splide__slide">
                            <div class="col-lg-12 col-12">
                                <h1 class="text-white">Cuma disini Pasang Iklan Serba Mudah & Tampil di Media Terkeren</h1>
                                <p class="text-white mt-4 mb-4">Pasang dan atur sendiri iklan sesukamu yang bisa kamu sesuaikan berdasarkan kebutuhan</p>
                                <a href="#pasang" class="btn-pasang-sekarang">
                                        Pasang Sekarang
                                    </a>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div class="col-lg-12 col-12">
                                <h1 class="text-white">Kami Menyediakan Cara Beriklan yang Bisa Kamu Sesuaikan</h1>
                                <p class="text-white mt-4 mb-4">Temukan berbagai kemudahan dan keuntungan dengan beriklan di Iklandisini.com</p>
                                <a href="#pasang" class="btn-pasang-sekarang">
                                        Pasang Sekarang
                                    </a>
                            </div>
                        </li>
                        <li class="splide__slide">
                            <div class="col-lg-12 col-12">
                                <h1 class="text-white">Kami Percaya Setiap Bisnis Berbeda, untuk itu Pasang & Atur Sendiri Iklanmu</h1>
                                <p class="text-white mt-4 mb-4">Layanan Pemasangan Iklan Secara Mandiri</p>
                                <a href="#pasang" class="btn-pasang-sekarang">
                                        Pasang Sekarang
                                    </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>

        </div>
    </div>
</header>
<!-- End Header  -->

<script>
    var splide = new Splide('.splide', {
        perPage: 1,
        rewind: true,
        arrows: false,
        autoplay: true,
        breakpoints: {
            991.98: {
                perPage: 1,
                gap: "1rem",
            },
            768: {
                perPage: 1,
                gap: "1rem",
            },
        },
    });

    splide.mount();
</script>