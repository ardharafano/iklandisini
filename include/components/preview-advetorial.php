<!-- Preview -->
<section class="preview mb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Preview Advetorial</h2>
                <span>Advetorial artikel sesuai kebutuhanmu</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/desktop.png" />
                    <span>
                        Desktop
                    </span>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/mobile.png" />
                    <span>
                        Mobile
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="?page=pasang-advetorial" class="btn-pasang-preview">
                    Pasang Sekarang
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Preview -->