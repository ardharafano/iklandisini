<!-- Layanan -->
<section class="baris p-md-5 px-2 py-5">
    <div class="bg-side-absolute"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">Iklan Baris</h2>
                <h6 class="text-center mb-5">Kami percaya setiap bisnis berbeda, untuk itu pasang & atur sendiri iklanmu</h6>
            </div>
        </div>
        <div class="row bg-custom">
            <div class="col-lg-4 col-12 d-flex justify-content-center">
                <div class="box">
                    <div class="thumb d-flex align-items-center justify-content-center flex-column">
                        <img src="assets/images/other-icons/layanan1.png" />
                    </div>
                    <h2>Pilih Sendiri</h2>
                    <span>Kamu bisa memilih sendiri jenis<br/>
                    iklan yang sesuai dengan<br/>
                    kebutuhan usahamu.</span>
                </div>
            </div>
            <div class="col-lg-4 col-12  d-flex justify-content-center">
                <div class="box">
                    <div class="thumb d-flex align-items-center justify-content-center flex-column">
                        <img src="assets/images/other-icons/layanan2.png" />
                    </div>
                    <h2>Pasang Sendiri</h2>
                    <span>Kamu yang paling mengerti<br/> 
                    konsumenmu. Pasang iklan<br/> 
                    sesuai kebutuhanmu.</span>
                </div>       
            </div>
            <div class="col-lg-4 col-12 d-flex justify-content-center">
                <div class="box">
                    <div class="thumb d-flex align-items-center justify-content-center flex-column">
                        <img src="assets/images/other-icons/layanan3.png" />
                    </div>
                    <h2>Pantau Sendiri</h2>
                    <span>Pantau status dan performa<br/> 
                    iklanmu di dashboard untuk<br/> 
                    digital & sosmed.</span>
                </div>   
            </div>
        </div>
    </div>
</section>
<!-- End Layanan -->