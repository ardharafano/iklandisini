<!-- Pasang Iklan  -->
<section class="pasang py-5" id="pasang">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">Pasang Iklan</h2>
                <h6 class="text-center mb-5">Kami menyediakan cara beriklan yang bisa kamu sesuaikan berdasarkan kebutuhan</h6>
            </div>
        </div>
        <div class="row d-flex flex-wrap justify-content-center">
            <div class="col-lg-13 col-sm-4 d-flex flex-wrap justify-content-center iklan">
                <div class="box">
                <h3 class="text-center mb-3">Display Banner</h3>
                    <img src="assets/images/other-icons/display6.png" alt="img" class="first" />
                    <span class="d-block text-center">Banyak pilihan iklan untuk media</span>
                    <a href="?page=desktop" class="btn-detail-display">
                        Selengkapnya
                    </a>
                </div>
            </div>
            <div class="col-lg-13 col-sm-4 d-flex flex-wrap justify-content-center iklan">
                <div class="box">
                <h3 class="text-center mb-3">Advetorial</h3>
                    <img src="assets/images/other-icons/display7.png" alt="img" />
                    <span class="d-block text-center">Iklan berupa berita</span>
                    <a href="?page=advetorial" class="mt-3 btn-detail-display">
                        Selengkapnya
                    </a>
                </div>
            </div>
            <div class="col-lg-13 col-sm-4 d-flex flex-wrap justify-content-center iklan">
                <div class="box">
                    <h3 class="text-center mb-3">Sosmed</h3>
                    <img src="assets/images/other-icons/display8.png" alt="img" />
                    <span class="d-block text-center">Banyak pilihan iklan untuk sosial media</span>
                    <a href="?page=sosmed" class="btn-detail-display">
                        Selengkapnya
                    </a>
                </div>
            </div>
            <div class="col-lg-13 col-sm-4 d-flex flex-wrap justify-content-center iklan">
                <div class="box">
                    <h3 class="text-center mb-3" style="position:relative;top:20px">Video</h3>
                    <img src="assets/images/other-icons/display9.png" alt="img" style="position:relative;top:20px" />
                    <span class="d-block text-center" style="position:relative;top:20px">Pembuatan Video dan lainnya</span>
                    <a href="?page=video" class="mt-3 btn-detail-display">
                        Selengkapnya
                    </a>
                </div>
            </div>
            <div class="col-lg-13 col-sm-4 d-flex flex-wrap justify-content-center iklan">
                <div class="box">
                    <h3 class="text-center mb-3">Display TV</h3>
                    <img src="assets/images/other-icons/display10.png" alt="img" />
                    <span class="d-block text-center">Pembuatan Display TV</span>
                    <a href="?page=tv" class="mt-3 btn-detail-display">
                        Selengkapnya
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Pasang Iklan  -->