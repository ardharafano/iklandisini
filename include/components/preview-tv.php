<!-- Preview -->
<section class="preview mb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Pembuatan Display TV</h2>
                <span>Lorem Ipsum is simply dummy text of the printing</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="box">
                    <img src="assets/images/examples/<?php echo $_GET['page']; ?>/tv_preview.png" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="?page=pasang-advetorial" class="btn-pasang-preview">
                    Pasang Sekarang
                </a>
            </div>
        </div>
    </div>
</section>
<!-- End Preview -->