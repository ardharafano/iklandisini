<!-- FAQ -->
<section class="faqs" id="faqs">
    <div class="container">
        <div class="wrap-s1">
            <h2 class="text-center">FAQs</h2>
            <h6 class="text-center mb-5">Masih punya pertanyaan seputar Iklan Digital?</h6>
            <div class="wrap-faq">
                <ul>
                    <li>
                        <label for="q1">
                            Berapa ukuran iklan Digital di Iklandisini.com?
                        </label>
                        <input type="checkbox" id="q1" class="q" />
                        <div class="wrap-answer">
                            <p>728x90, 160x600, 300x250, 300x600</p>
                        </div>
                    </li>
                    <li>
                        <label for="q2">
                            Dimana saja iklan akan muncul?
                        </label>
                        <input type="checkbox" id="q2" class="q" />
                        <div class="wrap-answer">
                            <p>Desktop, Mobile</p>
                        </div>
                    </li>
                    <li>
                        <label for="q3">
                            Seperti apa tampilan saya?
                        </label>
                        <input type="checkbox" id="q3" class="q" />
                        <div class="wrap-answer">
                            <p>Seperti preview iklan diatas</p>
                        </div>
                    </li>
                    <li>
                        <label for="q4">
                            Jika saya tidak punya website, apakah saya bisa memasang iklan?
                        </label>
                        <input type="checkbox" id="q4" class="q" />
                        <div class="wrap-answer">
                            <p>Bisa, karena kami sudah memiliki space iklan di beberapa media di Indonesia</p>
                        </div>
                    </li>
                    <li>
                        <label for="q5">
                            Metode pembayaran apa saja yang dapat digunakan ?
                        </label>
                        <input type="checkbox" id="q5" class="q" />
                        <div class="wrap-answer">
                            <p>Transfer</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>            
</section>
    <!-- End FAQ -->