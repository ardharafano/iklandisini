<header>
    <div class="d-flex flex-wrap align-content-stretch text-center">
        <div class="p-5 col-12 bg">

            <div class="container">
                <div class="row">
                    <div class="col-lg-9 text-start text-white">
                        <h1><b>Advetorial</b></h1>
                        <p class="fs-4">Kamu dapat mengiklankan sendiri usahamu melalui iklandisini.com, dan iklan akan ditanyangkan di website Arkadia Group: Suara.com, matamata.com, bolatimes.com, hitekno.com dewiku.com, mobimoto.com, guideku.com, himedik.com</p>
                    </div>

                    <div class="col-lg-3">
                        <img src="assets/images/header/display.png" class="img-header-components" alt="img" width="280px" height="280px" />
                    </div>
                </div>
            </div>

        </div>
    </div>
</header>