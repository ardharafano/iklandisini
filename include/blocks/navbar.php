<div id="nav">
    <nav class="navbar navbar-expand-xl navbar-light">
        <div class="container">
            <a class="navbar-brand" href="?page=home">
                <img src="assets/images/new-logo-iklandisini.svg" alt="iklandisini.com" width="191" height="23" style="position:relative;top:-5px" />
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-target="#myNavbar" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="?page=home#baris">Iklan Baris</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Pasang Iklan
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="?page=desktop">Display Banner</a></li>
                            <li><a class="dropdown-item" href="?page=advetorial">Advetorial</a></li>
                            <li><a class="dropdown-item" href="?page=sosmed">Sosmed</a></li>
                            <li><a class="dropdown-item" href="?page=video">Video</a></li>
                            <li><a class="dropdown-item" href="?page=tv">Display TV</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=home#keuntungan">Keuntungan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=home#harga">Harga</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=home#media">Media</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?page=home#faqs">FAQs</a>
                    </li>
                    <li class="nav-item right">
                        <a class="nav-link" href="?page=login" target="_blank">
                            <i class="icon-svg icon-login"></i> Masuk
                        </a>
                        <a class="nav-link register" href="?page=register" target="_blank">
                            Daftar
                        </a>
                    </li>
                    <li class="nav-item right2 dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            ID
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="?page=desktop">EN</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            IDR
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="?page=desktop">USD</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<!-- <script>
    $(document).ready(function(){
      // Add scrollspy to <body>
      $('body').scrollspy({target: ".navbar", offset: 150});   
    
      // Add smooth scrolling on all links inside the navbar
      $("#myNavbar a").on('click', function(event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();
    
          // Store hash
          var hash = this.hash;
    
          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){
       
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        }  // End if
      });
    });
    </script> -->