<a href="#" class="fly-wa-bottom">
    <img src="assets/images/wa-footer.png" alt="img" />
</a>
<footer class="footer">
    <div class="container-fluid bg-lavender py-5">
        <div class="container info">
            <div class="row">
                <div class="col-md-4 col-12 px-2">
                    <h5>Tentang Kami</h5>
                    <p>
                        Iklandisini.com merupakan platform pemasangan iklan mandiri 
                        (self-service adplatform) yang ditujukan bagi para pemilik 
                        usaha untuk memasarkan produk dan jasanya di media jaringan 
                        seluruh Indonesia. Para pemilik usaha dapat memasang iklan 
                        dengan mudah dan terjangkau di media digital, media cetak, 
                        maupun akun sosial media.
                    </p>
                </div>
                <div class="col-md-5 col-12 px-md-5 px-2 mb-3 mb-md-0">
                    <div class="row d-flex justify-content-md-center">
                        <div class="col-12 col-lg-5">
                            <h5>Jelajahi</h5>
                            <ul>
                                <li>
                                    <a href="#">
                                        Ketentuan Iklan
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Ketentuan Layanan
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Kebijakan Privasi
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Tentang Kami
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Iklan Digital
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Iklan Advetorial
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Iklan Sosmed
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Iklan Video
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-12 px-2 pembayaran">
                    <h5 class="mx-md-2 mx-0">Pembayaran</h5>
                    <img src="assets/images/other-logo/bca.png" class="m-2" alt="img" />
                    <img src="assets/images/other-logo/mandiri.png" class="m-2" alt="img" />
                    <img src="assets/images/other-logo/bni.png" class="m-2" alt="img" />
                    <img src="assets/images/other-logo/bri.png" class="m-2" alt="img" />
                    <img src="assets/images/other-logo/permata.png" class="m-2" alt="img" />
                    <img src="assets/images/other-logo/visa.png" class="m-2" alt="img" />
                    <img src="assets/images/other-logo/mastercard.png" class="m-2" alt="img" />
                    <div class="sosmed">
                        <a href="#" target="_blank" aria-label="sosmed">
                            <i class="icon-svg icon-fb"></i>
                        </a>
                        <a href="#" target="_blank" aria-label="sosmed">
                            <i class="icon-svg icon-tw"></i>
                        </a>
                        <a href="#" target="_blank" aria-label="sosmed">
                            <i class="icon-svg icon-ig"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid copyright">
        <div class="row">
            <div class="col-12 d-flex align-center justify-content-center py-3">
                <p class="text-center c-white copyright mb-0">&copy; 2021 Iklandisini.com - a subsidiary of Arkadia Digital Media</p>
            </div>
        </div>
    </div>
</footer>