const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const Webpack = require('webpack');

/* WebFont Generator */
// const webfontsGenerator = require('webfonts-generator');
// webfontsGenerator({
//     files: [
//         // 'src/icons/example.svg',
//     ],
//     dest: 'assets/webfonts/',
//     fontName: 'my-icon',
//     cssFontsUrl: '../webfonts/',
//     templateOptions: {
//         baseClass: 'icon',
//         classPrefix: 'icon-'
//     }
// }, function(error) {
//     if (error) {
//         console.log('Fail!', error);
//     } else {
//         console.log('Done!');
//     }
// });  
/* End WebFont Generator */


/* Css And JS Generate */
module.exports = {
    /* If There webfonts */
    // entry: ['./src/index.js', './assets/webfonts/my-icon.css', './src/sass/main.sass'],
    entry: ['./src/index.js', './src/sass/main.sass'],
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'assets/js/'),
    },
    mode: 'none',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader, 
                    'css-loader?url=false',
                ],
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader?url=false",
                    'sass-loader'
                ],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/env'],
                        plugins: ['@babel/plugin-proposal-class-properties']
                    }
                }
            },
            {
                test   : /\.(png|jpg|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                loader : 'file-loader',
            },
        ]
    },
    plugins: [
        new TerserPlugin(),
        new MiniCssExtractPlugin({
            filename: "../css/main.css",
        }),
        new Webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            Popper: ['popper.js', 'default']
       })
    ],
    optimization: {
        minimize: true,
        minimizer: [
            new CssMinimizerPlugin(),
        ],
    },
}
/* End Css And JS Generate */